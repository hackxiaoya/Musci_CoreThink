<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-12-24
 * Time: 1:43
 */
namespace Addons\SignatureCollection\Model;
use Think\Model;
/**
 * 个性签名采集模型
 */
class SignatureCollectionModel extends Model{
    /**
     * 数据库表名
     * @author yyyvy
     * 2017-12-24 01:51:20
     */
    protected $tableName = 'addon_signature_collection';

    /**
     * 后台列表管理相关定义
     * @author yyyvy
     * 2017-12-24 01:51:20
     */
    public $adminList = array(
      'title' => '采集数据列表',
      'model' => 'addon_signature_collection',
      'search_key'=>'contg',
      'order'=>'id desc',
      'map' => null,
      'list_grid' => array(
        'id' => array(
          'title' => 'ID',
          'type'  => 'text',
        ),
        'pid' => array(
          'title' => 'PID',
          'type'  => 'text',
        ),
        'contg' => array(
          'title' => '内容',
          'type'  => 'text',
        ),
        'status' => array(
          'title' => '状态',
          'type'  => 'status',
        ),
      ),
      'field' => array( //后台新增、编辑字段
        'contg' => array(
          'name'  => 'contg',
          'title' => '内容',
          'type'  => 'text',
          'tip'=>'内容',
        ),
      ),
      'but'=>array(
        'options'=>array(
          'release'=>array(
            'title'=>'release',
            'type'=>'AddTopButton',
            'url'=> array('_addons'=>'SignatureCollection','_controller'=>'SignatureCollection','_action'=>'release'),
          )
        )
      )
    );


    /**
     * 自动完成规则
     * @author yyyvy
     * 2017-12-24 01:51:20
     */
    protected $_auto = array(
      array('ctime', NOW_TIME, self::MODEL_INSERT),
      array('utime', NOW_TIME, self::MODEL_BOTH),
      array('status', '1', self::MODEL_INSERT),
    );
}