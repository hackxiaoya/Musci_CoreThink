DROP TABLE IF EXISTS `ct_addon_signature_collection`;

CREATE TABLE `ct_addon_signature_collection` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `pid` int(11) unsigned NOT NULL COMMENT '发布到哪一个栏目',
  `contg` varchar(255) NOT NULL DEFAULT '' COMMENT '内容',
  `ctime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `utime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='个性签名采集插件表';
