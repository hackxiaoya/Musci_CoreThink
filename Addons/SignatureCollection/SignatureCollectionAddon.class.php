<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Addons\SignatureCollection;
use Common\Controller\Addon;
/**
 * 个性签名采集插件
 * @author yyyvy
 * 2017-12-23 08:47:20
 */
class SignatureCollectionAddon extends Addon{
    /**
     * 插件信息
     * @author yyyvy
     * 2017-12-23 08:47:20
     */
    public $info = array(
        'name' => 'SignatureCollection',
        'title' => '个性签名采集插件',
        'description' => '个性签名采集',
        'status' => 1,
        'author' => 'yyyvy',
        'version' => '1.0'
    );

    /**
     * 其他按钮配置       otherbuttons
     * @author yyyvy
     * 2017-12-23 08:47:20
     */
    public $otherbuttons = array(
      '采集' => array(
        'title' => '采集列表',
        'controller' => 'signature_collection',     //控制器名称
      )
    );


    /**
     * 插件后台数据表配置
     * @author yyyvy
     * 2017-12-23 08:47:20
     */
    public $admin_list = array(
      '1' => array(
        'title' => '采集列表',
        'model' => 'signature_collection',
      )
    );

    /**
     * 插件安装方法
     * @author yyyvy
     * 2017-12-23 08:47:20
     */
    public function install(){
        return true;
    }

    /**
     * 插件卸载方法
     * @author yyyvy
     * 2017-12-23 08:47:20
     */
    public function uninstall(){
        return true;
    }
}
