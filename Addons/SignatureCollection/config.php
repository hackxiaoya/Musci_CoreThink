<?php
// +----------------------------------------------------------------------
// | HYEFM [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017 http://www.heyfm.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: yyyvy <2017-12-23 08:47:55> <http://www.heyfm.com>
// +----------------------------------------------------------------------
return array(
    'status'=>array(
        'title'=>'是否开启签名采集:',
        'type'=>'radio',
        'options'=>array(
            '1'=>'开启',
            '0'=>'关闭',
        ),
        'value'=>'1',
    ),
    'group'=>array(
        'type'=>'group',
        'options'=>array(
            'server'=>array(
                'title'=>'采集设置',
                'options'=>array(
                    'PID_TEPY'=>array(
                        'title'=>'发布分类ID：',
                        'type'=>'text',
                        'value'=>'',
                        'tip'=>'发布到哪一个分类的ID[如：1]',
                    ),
                    'URL_HOST'=>array(
                        'title'=>'采集地址：',
                        'type'=>'text',
                        'value'=>'http://www.heyfm.com',
                        'tip'=>'目标采集URL[如：http://www.heyfm.com]',
                    ),
                    'LIST_PAGE'=>array(
                        'title'=>'采集页数：',
                        'type'=>'text',
                        'value'=>'',
                        'tip'=>'?=page=2',
                    ),
                    'LIST_TITLE'=>array(
                        'title'=>'列表标题采集：',
                        'type'=>'text',
                        'value'=>'',
                        'tip'=>'.contit h2,text',
                    ),
                    'LIST_URL'=>array(
                        'title'=>'列表URL采集：',
                        'type'=>'text',
                        'value'=>'',
                        'tip'=>'.contit h2,text',
                    ),
                    'URL_CONTE'=>array(
                        'title'=>'内页内容采集：',
                        'type'=>'text',
                        'value'=>'',
                        'tip'=>'.contit h2,text',
                    ),
                    'URL_IMG'=>array(
                        'title'=>'内页图片采集：',
                        'type'=>'text',
                        'value'=>'',
                        'tip'=>'.contit h2,text',
                    )
                )
             )
        )
    )
);
