<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-12-24
 * Time: 1:47
 */
namespace Addons\SignatureCollection\Controller;
use Home\Controller\AddonController;
//  引入Querylist
require 'Addons/SignatureCollection/Querylist/phpQuery.php';
require 'Addons/SignatureCollection/Querylist/QueryList.class.php';
use QL\QueryList;
/**
 * 个性签名采集控制器
 * yyyvy
 * 2017-12-26 21:29:00
 */
class SignatureCollectionController extends AddonController{

    /*
     * 采集列表
     * yyyvy
     * 2017-12-26 23:44:26
     * */
    public function index($id){
        $listinfo = D('addon')->find($id);   //获取采集规则
        $db_config = json_decode($listinfo[config], true);
        $url_host = $db_config[URL_HOST];   //采集目标地址
        $list_title = array();  //定义列表标题空数组
        $list_url = array();    //定义列表URL空数组
        $listat = explode(',',$db_config[LIST_TITLE]);  //分割列表标题
        //循环出内容，压入列表标题数组
        for($i=0;$i<count($listat);$i++) {
            array_push($list_title,$listat[$i]);
        };
        $listau = explode(',',$db_config[LIST_URL]); //分割列表URL
        //循环出内容，压入列表URL数组
        for($i=0;$i<count($listau);$i++) {
            array_push($list_url,$listau[$i]);
        };
        //print_r ($db_config);die;
        $pattern = array("title" => $list_title,"url" => $list_url);
        $lists = QueryList::Query($url_host,$pattern,$outputEncoding = null, $inputEncoding = 'utf-8',$removeHead = false)->data;
        //print_r($lists);

        $arrurl = parse_url($url_host);  //拆分域名成数组
        $aurl = $arrurl[scheme] . '://' . $arrurl['host'];  //schem = http . '://' . host = 域名
        $result[id] = $id;  //返回id
        $result[pid] = $db_config[PID_TEPY];  //返回pid
        $result[aurl] = $aurl;  //返回域名URL
        $result[count] = count($lists);    //返回总数量

        $this->assign('meta_title', "采集列表");
        $this->assign('list', $lists);
        $this->assign('result', $result);
        $this->display(T('Addons://SignatureCollection@./Start'));
    }



    /*
     * 采集内页
     * yyyvy
     * 2017-12-26 23:44:26
     * */
    public function start(){
        $url = I('get.url');  //内页地址
        $listinfo = D('addon')->find(I('get.id'));   //获取采集规则
        $db_config = json_decode($listinfo[config], true);
        $url_conte = array();  //定义列表标题空数组
        $urlat = explode(',',$db_config[URL_CONTE]);  //分割列表标题
        //循环出内容，压入列表标题数组
        for($i=0;$i<count($urlat);$i++) {
            array_push($url_conte,$urlat[$i]);
        };
        $pattern = array("conte" => $url_conte);
        //$pattern = array("conte" => array('.content p','text','-font'));
        $list = QueryList::Query($url,$pattern,$outputEncoding = null, $inputEncoding = 'utf-8',$removeHead = false)->data;
        //$list = QueryList::Query('http://www.qqjay.com/html/rizhi/gaoxiao/195153.html',$pattern,$outputEncoding = null, $inputEncoding = 'utf-8',$removeHead = false)->data;
        //print_r ($pattern);die;
        foreach ($list as $v){
            preg_match_all('/[\x80-\xff]+/' , $v[conte] , $matches);
            $str = implode('',$matches[0]);
            //print_r($str);
            if($str){
                $map[contg] = array('LIKE', '%'.mb_substr($str,0,12).'%');
                $same = D('AddonSignatureCollection')->where($map)->find();   //获取内容
                $mapb[content] = array('LIKE', '%'.mb_substr($str,0,12).'%');
                $sameb = D('PublicAbeep')->where($mapb)->find();   //获取内容
                //print_r($sameb);die;
                if(!$same && !$sameb){
                    //新增文档
                    $collection_object = D('Addons://SignatureCollection/SignatureCollection');
                    //print_r($collection_object);die;
                    $result[pid] = I('get.pid');    //父级ID
                    $result[contg] = $str;    //内容
                    $results = $collection_object->create($result);
                    $sure = $collection_object->add($results);  //插入数据
                }
            }
        }
        //print_r($sure);exit;
        echo "200";
    }


    /*
     *  发布采集文档
     *  yyyvy
     *  2017-12-27 19:43:53
     * */
    public function release(){
        $ids = I('request.ids');
        if(empty($ids)){
            $this->error('请选择要操作的数据');
        }
        //AddonSignatureCollection
        //获取所有内容
        foreach ($ids as $v){
            $map['id'] = $v;
            $release_list = D('AddonSignatureCollection')
              ->where($map)
              //->fetchSql(true)
              ->select();
            //echo($document_list);exit;
            //print_r($release_list);

            /*  取数随机用户  */
            $user_map[group] = '2';     //分组是2
            $user_list = D('User')->where($user_map)->select(); //查询User数据库
            $userid = array();  //空数组
            foreach ($user_list as $aid){
                array_push($userid,$aid[id]);   //把ID压入数组
            }
            $RanResult = array_rand($userid,1); //随机取出一个

            /*  需要发布的内容 */
            $base_data[uid] = $userid[$RanResult];  //用户压入数组
            $base_data[content] = $release_list[0][contg];  //内容压入数组
            $base_data[pictures] = '';
            $base_data[ctime] = time();
            $base_data[utime] = time();
            $base_data[sort] = '0';
            $base_data[status] = '1';


            //调用文章模型，新增文档
            $document_object = D('PublicAbeep');   //模型
            $result = $document_object->add($base_data);
            //print_r($result);//die;
            if($result){
                $Collectionlist = M("AddonSignatureCollection");
                $Collectionlist->where('id='.$release_list[0][id])->delete(); // 删除数据
            }
        }

        //print_r($cid);die;
        if(!$result){
            $this->error($document_object->getError());
        }else{
            $this->success('新增成功', U('adminlist', array('name' => 'SignatureCollection')));
        }
    }
}