/* global mejs, _wpmejsSettings */
(function ($) {
	// add mime-type aliases to MediaElement plugin support
	mejs.plugins.silverlight[0].types.push('video/x-ms-wmv');
	mejs.plugins.silverlight[0].types.push('audio/x-ms-wma');

	$(function () {
		var settings = _wpmejsyuetingSettings || {},
			players = $('.wp-audio-shortcode, .wp-video-shortcode');

		settings.startVolume = parseFloat(settings.startvolume, 10);

		settings.alwaysShowControls = "true" === settings.controls;

		if ( $( document.body ).hasClass( 'mce-content-body' ) ) {
			return;
		}

		if ( typeof _wpmejsSettings !== 'undefined' ) {
			settings.pluginPath = _wpmejsSettings.pluginPath;
		}

		settings.success = function (mejs) {
			var autoplay = mejs.attributes.autoplay && 'false' !== mejs.attributes.autoplay;
			if ( 'flash' === mejs.pluginType && autoplay ) {
				mejs.addEventListener( 'canplay', function () {
					mejs.play();
				}, false );
			}
		};

		players.mediaelementplayer( settings );

		if ( "true" === settings.autoplay ) {
    		players.attr( 'autoplay', 'autoplay' );
    	};

	});

}(jQuery));

/*
Plugin Name: WP Audio Player
Plugin URI: http://www.thefox.cn/wp-audio-player.shtml
Description: 一款让你的WordPress站点立即成为播放视频和音频文件的强大插件。</strong>如果你觉得本插件给你带来的方便请<a href="http://me.alipay.com/aive"> 赞助我们</a></div>
Version: 6.0
Author: Aive Team
Author URI: http://www.weknow.cn
Author Email: nj@weknow.cn
*/