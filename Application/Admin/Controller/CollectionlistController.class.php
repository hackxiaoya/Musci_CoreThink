<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-12-08
 * Time: 3:22
 * 采集列表展示
 */

namespace Admin\Controller;


class CollectionlistController extends AdminController{
    /**
     * 默认方法
     * @author yyyvy
     * 2017-12-8 03:23:32
     */
    public function index($cid){
        //分类权限检测
        if(!D('UserGroup')->checkCategoryAuth($cid)){
            $this->error('权限不足！');
        }
        //搜索
        $keyword = I('keyword', '', 'string');
        $condition = array('like','%'.$keyword.'%');
        $map['id|title'] = array($condition, $condition,'_multi'=>true);

        $map['cid'] = $cid;
        //获取所有内容
        $collection_list = D('Collectionlist')->page(!empty($_GET["p"])?$_GET["p"]:1, C('ADMIN_PAGE_ROWS'))
          ->where($map)
          ->order('id desc')
          //->fetchSql(true)
          ->select();
        //echo($document_list);exit;

        $page = new \Common\Util\Page(D('Collectionlist')->where($map)->count(), C('ADMIN_PAGE_ROWS'));



        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->setMetaTitle('采集列表') //设置页面标题
          ->addTopButton('release',array('href' => U('admin/collectionlist/release'))) //添加发布按钮
          ->addTopButton('resume')  //添加启用按钮
          ->addTopButton('forbid')  //添加禁用按钮
          ->addTopButton('recycle') //添加回收按钮
          ->setSearch('请输入ID/标题', U('index', array('cid' => $cid)))
          ->addTableColumn('id', 'ID')
          ->addTableColumn('title', '标题', 'title')
          ->addTableColumn('ctime', '发布时间', 'time')
          ->addTableColumn('status', '状态', 'status')
          ->addTableColumn('right_button', '操作', 'btn')
          ->setTableDataList($collection_list) //数据列表
          ->setTableDataPage($page->show())  //数据列表分页
          ->addRightButton('edit')    //添加编辑按钮
          ->addRightButton('delete') //添加回收按钮
          ->display();
    }


    /**
     * 新增文档
     * @author yyyvy
     * 2017-12-8 03:56:15
     */
    public function add($cid){
        //分类权限检测
        if(!D('UserGroup')->checkCategoryAuth($cid)){
            $this->error('权限不足！');
        }
        if(IS_POST){
            //新增文档
            $collection_object = D('Collectionlist');
            $result = $collection_object->create();
            if(!$result){
                $this->error($collection_object->getError());
            }else{
                $id = $collection_object->add();
                //echo($id);die;
                $this->success('新增成功', U('Collectionlist/index', array('cid' => I('post.cid'))));
            }
        }else{
            $collection_object = D('Collectionlist');
            //获取当前分类
            $category_info = D('PublicQuerylist')->find($cid);



            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->setMetaTitle('新增文章')  //设置页面标题
            ->setPostUrl(U('add')) //设置表单提交地址
            ->addFormItem('cid', 'hidden')
            ->setFormData(array('cid' => $category_info['id']))
            ->addFormItem('title', 'textarea', '标题', '标题')
            ->addFormItem('contg', 'textarea', '内容', '内容')
            ->addFormItem('cover', 'picture', '封面图片', '封面图片')
            ->addFormItem('covers', 'pictures', '图片', '图片')
            ->display();
        }
    }

    /**
     * 编辑文章
     * @author jry <598821125@qq.com>
     */
    public function edit($id){

        //分类权限检测
        if(!D('UserGroup')->checkCategoryAuth($document_info['cid'])){
            $this->error('权限不足！');
        }

        if(IS_POST){
            //更新文档
            $collection_object = D('Collectionlist');
            $result = $collection_object->create();
            //print_r($result);die;
            if($result){
                if($collection_object->save()!== false){
                    $this->success('更新成功', U('Collectionlist/index', array('cid' => I('post.cid'))));
                }else{
                    $this->error('更新失败');
                }
            }else{
                $this->error($collection_object->getError());
            }
        }else{
            $document_object = D('Collectionlist');

            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->setMetaTitle('编辑文章') //设置页面标题
            ->setPostUrl(U('edit')) //设置表单提交地址
            ->addFormItem('id', 'hidden', 'ID', 'ID')
              ->addFormItem('cid', 'hidden', 'CID', 'CID')
              ->addFormItem('title', 'textarea', '标题', '标题')
              ->addFormItem('contg', 'textarea', '内容', '内容')
              ->addFormItem('cover', 'picture', '封面图片', '封面图片')
              ->addFormItem('covers', 'pictures', '图片', '图片')
              ->setFormData(D('Collectionlist')->find($id))
              ->display();
        }
    }



    /*
     *  发布采集文档
     *  yyyvy
     *  2017-12-14 02:47:29
     * */
    public function release(){
        $ids = I('request.ids');
        if(empty($ids)){
            $this->error('请选择要操作的数据');
        }

        $cid = M("Collectionlist");
        $cid = $cid->where('id='.I('request.ids')[0])->find(); // 查询
        //获取所有内容
        foreach ($ids as $v){
            $map['id'] = $v;
            $release_list = D('Collectionlist')
              ->where($map)
              //->fetchSql(true)
              ->select();
            //echo($document_list);exit;
            //print_r($release_list);

            //取出发布文档的分类PID
            $doc_type = D('PublicQuerylist')->find($release_list[0][cid]);
            $pid = $doc_type[pid];  //分类文档PID
            $release_list[0][cid] = $pid;       //把采集分类替换成需要发布的分类
            //print_r($release_list);die;


            //调用文章模型，新增文档
            $document_object = D('Document');   //模型
            $result = $document_object->CollectionuUpdate($release_list[0]);    //调用函数
        }

        //print_r($cid);die;
        if(!$result){
            $this->error($document_object->getError());
        }else{
            $this->success('新增成功', U('Collectionlist/index', array('cid' => $cid[cid])));
        }
    }

}