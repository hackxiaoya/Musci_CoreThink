<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-12-13
 * Time: 2:34
 */

namespace Admin\Controller;
use Think\Controller;
/*
 * 采集用户
 * By:xiaoya
 * 2017-12-13 02:36:27
 * */

class useravatarController extends AdminController{

    public function index($page=''){
        $Page = $page;
        import('Common.Util.Querylist.QueryList');	//引入Querylist
        $pattern = array("url" => array(".list li .avatar a","href"));
        $url = 'http://www.qzone.cc/top/rank/'.$Page;    //页数
        $arrurl = parse_url($url);  //拆分域名成数组
        $aurl = $arrurl[scheme] . '://' . $arrurl['host'];  //schem = http . '://' . host = 域名
        $qy = new \QueryList($url, $pattern, '', '', '');
        $list = $qy->jsonArr;

        $result[count] = count($list);    //返回总数量
        $this->assign('meta_title', "采集列表");
        $this->assign('list', $list);
        $this->assign('result', $result);
        $this->display();
    }


    public function Startcontg(){
        import('Common.Util.Querylist.QueryList');	//引入Querylist
        $url = I('get.url');  //内页地址
        $bu = 'http://www.qzone.cc';
        $buu = $bu.$url;
        //$accc = 'http://www.qzone.cc/u/1576606';
        $pattern = array(
          "title" => array(".member_center01 .member_info .c .ptitle","text"),  //,"-.sex_women span"
          "avatar" => array(".member_center01 .l p img","src"),
          "usersign" => array(".member_center01 .c .pmysay .usersign","text")
        );
        $qy = new \QueryList($buu, $pattern, '', '', '');
        $rs = $qy->jsonArr;
        //print_r($rs[0][avatar]);
        //echo json_encode($rs);


        $strimgurl = $rs[0][avatar];   //截取.jpg之前的内容
        $dir = "./Uploads/".date('Y-m-d');
        if(!is_dir($dir)) {
            mkdir("./Uploads/".date('Y-m-d'),0777,true);    //创建目录
        }
        $Remote_download = R('PublicQuerylist/download_img',array($strimgurl,$dir));    //调用curl下载图片类
        //新增图片
        $img_object = D('PublicUpload');
        $imgresults = $img_object->create($Remote_download);
        $avatarid = $img_object->add($imgresults);  //插入数据


        //新增采集用户
        $result[reg_type] = '2';
        $result[usertype] = '0';
        $result[group] = '2';
        $result[username] = $rs[0][title];
        $result[email] = 'collection@heyfm.com';
        $result[mobile] = '13145202346';
        $result[password] = user_md5('aa123456');
        $result[avatar] = $avatarid;
        $result[vip] = '0';
        $result[summary] = isset($rs[0][usersign]) ? $rs[0][usersign] : '';
        $result[status] = '1';
        $result[ctime] = time();
        $result[utime] = time();

        $user_object = D('User');   //调用user模型
        $results = $user_object->create($result);
        $data = $user_object->add($result);    //新增
        echo json_encode($data);
    }

}