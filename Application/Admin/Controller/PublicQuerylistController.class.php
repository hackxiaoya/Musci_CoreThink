<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-12-05
 * Time: 1:24
 */


namespace Admin\Controller;
use Think\Controller;

/*
 * QueryList采集器
 * By:xiaoya
 * 2017-12-5 01:26:57
 * */
class PublicQuerylistController extends AdminController{
    public function index(){
        //搜索
        $keyword = I('keyword', '', 'string');
        $condition = array('like','%'.$keyword.'%');
        $map['id|title'] = array($condition, $condition,'_multi'=>true);

        //获取所有规则
        $data_list = D('PublicQuerylist')->page(!empty($_GET["p"])?$_GET["p"]:1, C('ADMIN_PAGE_ROWS'))
          ->where($map)
          ->order('id desc')
          //->fetchSql(true)
          ->select();
        //print_r($data_list);exit;

        $page = new \Common\Util\Page(D('PublicQuerylist')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

        foreach($data_list as &$item){
            $item['title'] = '<a href="'.U('Collectionlist/index', array('cid' => $item['id'])).'">'.$item['title'].'</a>';
        }



        //使用Builder快速建立列表页面。
        $builder = new \Common\Builder\ListBuilder();
        $builder->setMetaTitle('规则列表') //设置页面标题
        ->addTopButton('addnew')  //添加新增按钮
        ->addTopButton('resume')  //添加启用按钮
        ->addTopButton('forbid')  //添加禁用按钮
        ->addTopButton('delete')  //添加删除按钮
        ->setSearch('请输入ID/评论关键字', U('index'))
          ->addTableColumn('id', 'ID')
          ->addTableColumn('title', '标题')
          ->addTableColumn('ctime', '创建时间', 'time')
          ->addTableColumn('utime', '更新时间', 'time')
          ->addTableColumn('right_button', '操作', 'btn')
          ->setTableDataList($data_list) //数据列表
          ->setTableDataPage($page->show()) //数据列表分页
          ->addRightButton('startlist') //添加采集按钮
          ->addRightButton('edit')   //添加编辑按钮
          ->addRightButton('delete') //添加删除按钮
          ->display();
    }


    /**
     * 新增采集
     * 2017-11-24 23:34:46
     */
    public function add(){

        if(IS_POST){
            $user_comment_object = D('PublicQuerylist');
            $data = $user_comment_object->create();
            //->fetchSql(true)
            //print_r ($user_comment_object);exit;
            if($data){
                $id = $user_comment_object->add();
                if($id){
                    $this->success('新增成功', U('index'));
                }else{
                    $this->error('新增失败');
                }
            }else{
                $this->error($user_comment_object->getError());
            }
        }else{
            $user_comment_object = D('PublicQuerylist');

            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->setMetaTitle('新增采集')  //设置页面标题
            ->setPostUrl(U('add')) //设置表单提交地址
            ->addFormItem('pid', 'select', '发布栏目', '发布到什么栏目', select_list_as_tree('Category', array('group' => '1')))
              ->addFormItem('title', 'textarea', '标题', '标题')
              ->addFormItem('url', 'textarea', '目标URL', '目标URL')
              ->addFormItem('page', 'textarea', '页数', '页数')
              ->addFormItem('listtitle', 'textarea', '列表标题', '.contit h2,text')
              ->addFormItem('listurl', 'textarea', '列表URL', '.contit h2,href')
              ->addFormItem('contgtitle', 'textarea', '内页标题', '.contit h2,text')
              ->addFormItem('contg', 'textarea', '内页内容', '.contit h2,text')
              ->addFormItem('contgimg', 'textarea', '内页图片', '.contit h2,src')
              ->display();
        }
    }


    /**
     * 编辑采集
     * 2017-11-24 23:34:53
     */
    public function edit($id){
        if(IS_POST){
            $user_comment_object = D('PublicQuerylist');
            $data = $user_comment_object->create();
            if($data){
                if($user_comment_object->save()!== false){
                    $this->success('更新成功', U('index'));
                }else{
                    $this->error('更新失败');
                }
            }else{
                $this->error($user_comment_object->getError());
            }
        }else{
            $user_comment_object = D('PublicQuerylist');

            //使用FormBuilder快速建立表单页面。
            $builder = new \Common\Builder\FormBuilder();
            $builder->setMetaTitle('编辑采集')  //设置页面标题
            ->setPostUrl(U('edit')) //设置表单提交地址
            ->addFormItem('id', 'hidden', 'ID', 'ID')
              ->addFormItem('pid', 'select', '发布栏目', '发布到什么栏目', select_list_as_tree('Category', array('group' => '1')))
              ->addFormItem('title', 'textarea', '标题', '标题')
              ->addFormItem('url', 'textarea', '目标URL', '目标URL')
              ->addFormItem('page', 'textarea', '页数', '页数')
              ->addFormItem('listtitle', 'textarea', '列表标题', '.contit h2,text')
              ->addFormItem('listurl', 'textarea', '列表URL', '.contit h2,href')
              ->addFormItem('contgtitle', 'textarea', '内页标题', '.contit h2,text')
              ->addFormItem('contg', 'textarea', '内页内容', '.contit h2,text')
              ->addFormItem('contgimg', 'textarea', '内页图片', '.contit h2,src')
              ->setFormData(D('PublicQuerylist')->find($id))
              ->display();
        }
    }



    /**
     *  获取采集列表  collection
     *  yyyvy
     *  2017-12-9 23:15:21
     *
     **/
    public function Startlist($id){
        import('Common.Util.Querylist.QueryList');	//引入Querylist
        $listinfo = D('PublicQuerylist')->find($id);   //获取采集规则
        $at = array();
        $au = array();
        $listat = explode(',',$listinfo[listtitle]);
        //循环出内容，压入数组
        for($i=0;$i<count($listat);$i++)
        {
            array_push($at,$listat[$i]);
        }
        $listau = explode(',',$listinfo[listurl]);
        for($i=0;$i<count($listau);$i++)
        {
            array_push($au,$listau[$i]);
        }
        //print_r ($page);die;
        $pattern = array("title" => $at,"url" => $au);
        $url = $listinfo[url];
        $page = $listinfo[page];    //页数
        $arrurl = parse_url($url);  //拆分域名成数组
        //print_r($url.$page);die;
        $aurl = $arrurl[scheme] . '://' . $arrurl['host'];  //schem = http . '://' . host = 域名
        //$qy = new \QueryList($url, $pattern, '', '', 'UTF-8');
        $qy = new \QueryList($url.$page, $pattern, '', '', '');
        $list = $qy->jsonArr;


        //print_r($list);exit;

        //判断是否采集过
        foreach(array_keys($list) as $key ) {
            $ttta = '0';  //定义一个变量用于判断是否存在标题
            foreach ($list[$key] as $v){
                //mb_substr($v,0,7,);    //截取标题长度
                $map[title] = array('LIKE', '%'.mb_substr($v,0,9).'%');
                $TitleIf = D('Collectionlist')->where($map)->find();   //获取标题
                //print_r($TitleIf);exit;
                $TitleIf2 = D('DocumentGallery')->where($map)->find();   //获取标题
                //存在返回1
                if($TitleIf || $TitleIf2){
                    $ttta = '1';
                }
            };
            //判断存在，根据数组下标 删除数组
            if($ttta == 1){
                unset($list[$key]);
            }
        };


        $lists = array();
        //替换数组里包含的换行符
        foreach ($list as $v){
            $str[title] = preg_replace('/\r|\n/', '', $v[title]);
            $str[title] = stripslashes($str[title]);
            $str[url] = $v[url];
            //print_r($v);
            array_push($lists,$str);
        };
        /*print_r($list);
        exit;*/
        /*print_r($lists);
        exit;*/
        $result = array();    //返回数组
        $result[cid] = $id;  //返回cid
        $result[aurl] = $aurl;  //返回域名URL
        $result[count] = count($lists);    //返回总数量

        $this->assign('meta_title', "采集列表");
        $this->assign('list', $lists);
        $this->assign('result', $result);
        $this->display();
    }

    /**
     *  下载图片
     *  yyyvy
     *  2017-12-11 23:52:07
     *  $url    是图片完全网络路径http://www.heyfm.com/2017/123.jpg
     *  $filename   是存储全路径./Uploads/2017
     * */
    public function download_img($url = "", $filename = ""){
        $strimgurl = strstr($url,".jpg",true).'.jpg';   //截取.jpg之前的内容
        $strimgname = basename($strimgurl);   //截取图片名xxxx.jpg
        $strimgexit = explode('.',$strimgname);   //截取图片名
        $strimgexita = $strimgexit[1];    //.jpg / .png     图片类型
        $ch = curl_init(); //初始化一个curl句柄
        $filenames = $filename.'/'.$strimgname;
        $hd = fopen($filenames,'wb');     //写入文件路径
        curl_setopt($ch,CURLOPT_URL,$url); //需要获取的 URL 地址
        curl_setopt($ch,CURLOPT_FILE,$hd); //设置成资源流的形式
        curl_setopt($ch,CURLOPT_HEADER,0); //启用时会将头文件的信息作为数据流输出。
        curl_setopt($ch,CURLOPT_TIMEOUT,60); //设置超时时间
        curl_exec($ch); //执行curl
        curl_close($ch); //关闭curl会话
        fclose($hd); //关闭句柄
        $Remote_download['name'] = $strimgname;
        $Remote_download['path'] = $filenames;
        $Remote_download['url'] = '';
        $Remote_download['ext'] = $strimgexita;
        $Remote_download['size'] = filesize($filenames);
        $Remote_download['md5']  = md5_file($filenames);
        $Remote_download['sha1']  = sha1_file($filenames);
        $Remote_download['location']  = 'RemoteDownload';

        return $Remote_download;
    }
    /**
     *  获取采集内容  collection
     *  yyyvy
     *  2017-12-9 23:15:21
     *
     **/
    public function Startcontg(){
        import('Common.Util.Querylist.QueryList');	//引入Querylist
        $url = I('get.url');  //内页地址
        $listinfo = D('PublicQuerylist')->find(I('get.cid'));   //获取采集规则
        $at = array();
        $au = array();
        $listat = explode(',',$listinfo[contgtitle]);
        for($i=0;$i<count($listat);$i++) {
            array_push($at,$listat[$i]);
        }
        $listau = explode(',',$listinfo[contgimg]);
        for($i=0;$i<count($listau);$i++) {
            array_push($au,$listau[$i]);
        }
        $pattern = array("title" => $at, "img" => $au);
        $qy = new \QueryList($url, $pattern, '', '', '');
        $rs = $qy->jsonArr;

        //print_r($rs);exit;

        $lists = array();   //过滤数组内容，提前申明
        $imgresult=array();     //图片空数组
        foreach ($rs as $v){
            $str[title] = preg_replace('/\r|\n/', '', $v[title]);
            $str[title] = stripslashes($str[title]);
            $str[img] = $v[img];
            //print_r($v);
            array_push($lists,$str);
        };
        /*print_r($lists);
        exit;*/

        foreach ($lists as $value){
            $strimgurl = strstr($value[img],".jpg",true).'.jpg';   //截取.jpg之前的内容
            $imgurl = $strimgurl;
            $dir = "./Uploads/".date('Y-m-d');
            if(!is_dir($dir)) {
                mkdir("./Uploads/".date('Y-m-d'),0777,true);    //创建目录
            }
            $Remote_download = $this->download_img($imgurl,$dir);    //调用curl下载图片类
            //新增图片入数据库
            $img_object = D('PublicUpload');
            $imgresults = $img_object->create($Remote_download);
            $id = $img_object->add($imgresults);  //插入数据
            array_push($imgresult,$id);
        }
        //exit;
        $covers=implode(',',$imgresult);        //多图集
        //print_r($c);exit;



        //新增文档
        $collection_object = D('Collectionlist');
        $result[cid] = I('get.cid');    //父级ID
        $result[title] = $lists[0][title];    //标题
        $result[contg] = $lists[0][contg];    //内容
        $result[cover] = [];            //封面
        $result[covers] = $covers;           //多图
        $results = $collection_object->create($result);
        $sure = $collection_object->add($results);  //插入数据
        //print_r($sure);exit;
        if($sure){
            echo ('200');
        }else{
            echo ('400');
        }
    }
}