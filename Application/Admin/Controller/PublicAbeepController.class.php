<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-11-24
 * Time: 23:19
 */

namespace Admin\Controller;
use Think\Controller;
/**
 * 后台一哔控制器
 * 2017-11-24 23:20:32
 */
class PublicAbeepController extends AdminController{
    /**
     * 一哔列表
     * 2017-11-24 23:20:58
     */
    public function index(){
      //搜索
      $keyword = I('keyword', '', 'string');
      $condition = array('like','%'.$keyword.'%');
      $map['id|content'] = array($condition, $condition,'_multi'=>true);

      //获取所有评论
      $map['status'] = array('egt', '0'); //禁用和正常状态
      $data_list = D('PublicAbeep')->page(!empty($_GET["p"])?$_GET["p"]:1, C('ADMIN_PAGE_ROWS'))
        ->where($map)
        ->order('sort desc,id desc')
        ->select();
      $page = new \Common\Util\Page(D('PublicAbeep')->where($map)->count(), C('ADMIN_PAGE_ROWS'));

      //使用Builder快速建立列表页面。
      $builder = new \Common\Builder\ListBuilder();
      $builder->setMetaTitle('一哔列表') //设置页面标题
      ->addTopButton('addnew')  //添加新增按钮
      ->addTopButton('resume')  //添加启用按钮
      ->addTopButton('forbid')  //添加禁用按钮
      ->addTopButton('delete')  //添加删除按钮
      ->setSearch('请输入ID/评论关键字', U('index'))
      ->addTableColumn('id', 'ID')
      ->addTableColumn('content', '内容')
      ->addTableColumn('ctime', '创建时间', 'time')
      ->addTableColumn('sort', '排序')
      ->addTableColumn('status', '状态', 'status')
      ->addTableColumn('right_button', '操作', 'btn')
      ->setTableDataList($data_list) //数据列表
      ->setTableDataPage($page->show()) //数据列表分页
      ->addRightButton('edit')   //添加编辑按钮
      ->addRightButton('forbid') //添加禁用/启用按钮
      ->addRightButton('delete') //添加删除按钮
      ->display();
    }

    /**
     * 新增一哔
     * 2017-11-24 23:34:46
     */
    public function add(){
      if(IS_POST){
        $user_comment_object = D('PublicAbeep');
        $data = $user_comment_object->create();
        if($data){
          $id = $user_comment_object->add();


          if($id){
            $this->success('新增成功', U('index'));
          }else{
            $this->error('新增失败');
          }
        }else{
          $this->error($user_comment_object->getError());
        }
      }else{
        $user_comment_object = D('PublicAbeep');

        //使用FormBuilder快速建立表单页面。
        $builder = new \Common\Builder\FormBuilder();
        $builder->setMetaTitle('新增一哔')  //设置页面标题
        ->setPostUrl(U('add')) //设置表单提交地址
        ->addFormItem('content', 'textarea', '评论内容', '评论内容')
        ->addFormItem('pictures', 'pictures', '图片列表', '图片列表')
        ->addFormItem('rate', 'num', '评分', '评分')
        ->addFormItem('sort', 'num', '排序', '用于显示的顺序')
        ->display();
      }
    }


    /**
     * 编辑一哔
     * 2017-11-24 23:34:53
     */
    public function edit($id){
      if(IS_POST){
        $user_comment_object = D('PublicAbeep');
        $data = $user_comment_object->create();
        if($data){
          if($user_comment_object->save()!== false){
            $this->success('更新成功', U('index'));
          }else{
            $this->error('更新失败');
          }
        }else{
          $this->error($user_comment_object->getError());
        }
      }else{
        $user_comment_object = D('PublicAbeep');

        //使用FormBuilder快速建立表单页面。
        $builder = new \Common\Builder\FormBuilder();
        $builder->setMetaTitle('编辑一哔')  //设置页面标题
        ->setPostUrl(U('edit')) //设置表单提交地址
        ->addFormItem('id', 'hidden', 'ID', 'ID')
        ->addFormItem('content', 'textarea', '评论内容', '评论内容')
        ->addFormItem('pictures', 'pictures', '图片列表', '图片列表')
        ->addFormItem('rate', 'num', '评分', '评分')
        ->addFormItem('sort', 'num', '排序', '用于显示的顺序')
        ->setFormData(D('PublicAbeep')->find($id))
        ->display();
      }
    }

}