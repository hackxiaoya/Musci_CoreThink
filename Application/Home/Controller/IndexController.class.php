<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
namespace Home\Controller;
use Think\Controller;
/**
 * 前台默认控制器
 * @author jry <598821125@qq.com>
 */
class IndexController extends HomeController{

	


    /**
     * 默认方法
     * @author jry <598821125@qq.com>
     */
    public function index(){
        Cookie('__forward__', $_SERVER['REQUEST_URI']);
        $this->assign('meta_title', "首页");
        $this->display('');
    }


	/*
	 * description：Ajax播放音乐
	 * author：Xiaoya
	 * time：2016-4-2 18:57:09
	 */
	public function ajax(){
		$id=intval(I('id'));
		//获取ID
		$info = D('Document')->where($map)->detail($id);
		$ajax['id']=$id;
		$ajax['ids']=$id;
		//获取标题
		$ajax['title']=$info['title'];
		//获取发布者
		$username = D('user')->find($info['uid']);
		$ajax['username']=$username['username'];
		//获取MP3地址
        $mp3 = D('PublicUpload')->find($info['mp3']);
        $ajax['mp3'] = $mp3['path'];
		$ajax['artist'] = str_replace(".mp3", "", $mp3['name']);
		//获取封面
		$cover = D('PublicUpload')->find($info['cover']);
        $ajax['poster'] = $cover['path'];
		//获取歌词
		$lyric = D('Document_music_list')->find($id);
		$ajax['lyric'] = $lyric['lyric'];
		$_b[]=$ajax;
		$this->ajaxReturn($_b);
	}
}
