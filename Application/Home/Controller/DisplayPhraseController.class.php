<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2016/5/18
 * Time: 19:55
 */
namespace Home\Controller;
use Think\Controller;
use Common\Util;
/**
 * 文章控制器
 * @author xiaoya <31904431@qq.com>
 * 2016-05-18 19:58:51
 * 路径：/index.php?s=/home/DisplayPhrase/Phraselist
 */
class DisplayPhraseController extends HomeController{
    /**
     *  Description: 小站哔哔ajax发布
     *  Date: 2016-5-25 20:07:28
     *  By: Xiaoya
     */
    public function add()
    {
        $userinfo=$this->is_login();
        $obj=D('Phrase');
        $map['uid']=$userinfo['id'];    //用户ID
        $map['pic']=$_POST['pic'];  //图片路径
        $map['content']=$_POST['content'];  //内容
        $map['ctime']=time();   //时间
        $obj->add($map);    //插入数据库
        $this->ajaxReturn('添加成功');  //返回json
    }

    /**
     *  Description: 小站哔哔展示页面
     *  Date: 2016-5-25 20:07:33
     *  By: Xiaoya
     */
    public function Phraselist()
    {
        //获取分类信息
        $template = 'Document/index_Phrase';  //模板
        $document_list = D('Phrase')->page(!empty($_GET["p"]) ? $_GET["p"] : 1, C('ADMIN_PAGE_ROWS'))
            ->order('ctime desc')
            ->where($map)
            //->fetchSql(true)
            ->select();
        /*echo ($document_list);
        exit;*/
        foreach ($document_list as &$val) {

        }

        $this->assign('volist', $document_list);
        Cookie('__forward__', $_SERVER['REQUEST_URI']);
        $this->display($template);
    }
}