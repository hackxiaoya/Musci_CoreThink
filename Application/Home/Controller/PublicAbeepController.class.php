<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-11-24
 * Time: 23:12
 */

namespace Home\Controller;
use Think\Controller;


/**
 *  一哔控制器
 *  2017-11-24 23:13:27
 *
 */
class PublicAbeepController extends HomeController{

    /**
     *  Description: 一哔展示页面
     *  Date: 2016-4-9 23:09:00
     *  By: Xiaoya
     */
    public function index(){
      //获取分类信息
      $template = 'Document/index_abeep';  //模板
      $abeep_list = D('PublicAbeep')->page(!empty($_GET["p"])?$_GET["p"]:1, C('ADMIN_PAGE_ROWS'))
        ->order('ctime desc,ctime desc')
        ->where($map)
        //->fetchSql(true)
        ->select();
      /*echo ($document_list);
      exit;*/

      $this->assign('volist', $abeep_list);
      Cookie('__forward__', $_SERVER['REQUEST_URI']);
      $this->display($template);
    }


    /**
     *  Description: 一哔ajax发布
     *  Date: 2016-4-9 22:02:58
     *  By: Xiaoya
     */
    public function add(){
      if(IS_POST){
        $uid = $this->is_login();
        $public_comment_object = D('PublicAbeep');
        $data = $public_comment_object->create();
        if($data){
          $id = $public_comment_object->add();
          if($id){
            $this->success('发表成功');
          }else{
            $this->error('发表失败');
          }
        }else{
          $this->error($public_comment_object->getError());
        }
      }

    }



}