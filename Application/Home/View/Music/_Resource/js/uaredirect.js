/*
*     手机端 PC端判断
*     2017-11-17 01:05:01
*     yyyvy
*     引用代码：
*   <script src="你的js存放目录/uaredirect.js" type="text/javascript"></script>
*   <script type="text/javascript">uaredirect("http://m.xxx.com","http://www.xxx.com");</script>
*
* */
function uaredirect(f) {
  try {
    if (document.getElementById("bdmark") != null) {
      return;
    }
    var b = false;
    if (arguments[1]) {
      var e = window.location.host;
      var a = window.location.href;
      if (isSubdomain(arguments[1], e) == 1) {
        f = f + "/#m/" + a;
        b = true
      } else {
        if (isSubdomain(arguments[1], e) == 2) {
          f = f + "/#m/" + a;
          b = true
        } else {
          f = a;
          b = false
        }
      }
    } else {
      b = true
    }
    if (b) {
      var c = window.location.hash;
      if (!c.match("fromapp")) {
        if ((navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i))) {
          location.replace(f)
        }
      }
    }
  } catch(d) {}
}
function isSubdomain(c, d) {
  this.getdomain = function(f) {
    var e = f.indexOf("://");
    if (e > 0) {
      var h = f.substr(e + 3)
    } else {
      var h = f
    }
    var g = /^www\./;
    if (g.test(h)) {
      h = h.substr(4)
    }
    return h
  };
  if (c == d) {
    return 1
  } else {
    var c = this.getdomain(c);
    var b = this.getdomain(d);
    if (c == b) {
      return 1
    } else {
      c = c.replace(".", "\\.");
      var a = new RegExp("\\." + c + "$");
      if (b.match(a)) {
        return 2
      } else {
        return 0
      }
    }
  }
};