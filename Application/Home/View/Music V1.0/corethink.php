<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
//主题信息配置
return array(
    //主题信息
    'info' => array(
        'name'        => 'Music',
        'title'       => '音乐主题',
        'description' => 'Music音乐主题',
        'developer'   => 'XiaoYa',
        'website'     => 'http://www.corethink.cn',
        'version'     => '1.0',
    ),

    //主题配置
    'config' => array(),
);
