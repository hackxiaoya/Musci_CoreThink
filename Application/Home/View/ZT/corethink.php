<?php
// +----------------------------------------------------------------------
// | CoreThink [ Simple Efficient Excellent ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.corethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: jry <598821125@qq.com> <http://www.corethink.cn>
// +----------------------------------------------------------------------
//主题信息配置
return array(
    //主题信息
    'info' => array(
        'name'        => 'ZT',
        'title'       => '中天盛世官方模板',
        'description' => '中天盛世官方模板',
        'developer'   => 'yyyvy',
        'website'     => 'http://www.baidu.com',
        'version'     => '1.0',
    ),

    //主题配置
    'config' => array(),
);
