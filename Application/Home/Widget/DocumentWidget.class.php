<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2016/3/27
 * Time: 0:21
 */
namespace Home\Widget;
use Think\Controller;

class DocumentWidget extends Controller{
    //判断是否已经收藏
    public function isDocCollect($uid,$docid)
    {
        $map['uid'] = $uid;
        $map['docid'] = $docid;
        if (empty($docid)) {
            return false;
        }
        $data = D('Collect')->where($map)->find();
        if (empty($data)) {
            echo 'fa-heart-o';
        }else{
            echo 'fa-heart';
        }
    }
}