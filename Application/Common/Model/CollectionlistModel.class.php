<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-11-25
 * Time: 0:24
 */

namespace Common\Model;
use Think\Model;


/**
 * 一哔模型
 * @author yyyvy
 * 2017-11-25 00:26:56
 */
class CollectionlistModel extends Model{

  /**
   * 自动完成规则
   * @author yyyvy
   * 2017-11-25 00:26:56
   */
  protected $_auto = array(
    array('uid', 'is_login', self::MODEL_INSERT, 'function'),
    array('ctime', 'time', self::MODEL_INSERT, 'function'),
    array('utime', 'time', self::MODEL_BOTH, 'function'),
    array('status', 1, self::MODEL_INSERT, 'string'),
  );


}