<?php
/**
 * Created by PhpStorm.
 * User: Xiaoya
 * Date: 2017-12-08
 * Time: 2:53
 */

namespace Common\Model;
use Think\Model;

/**
 * 采集模型
 * @author yyyvy
 * 2017-12-8 02:54:40
 */
class PublicQuerylistModel extends Model{

    /**
     * 自动完成规则
     * @author yyyvy
     * 2017-12-8 02:55:17
     */
    protected $_auto = array(
      array('uid', 'is_login', self::MODEL_INSERT, 'function'),
      array('ctime', 'time', self::MODEL_INSERT, 'function'),
      array('utime', 'time', self::MODEL_BOTH, 'function'),
    );


}